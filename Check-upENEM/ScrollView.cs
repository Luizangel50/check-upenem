using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace CheckupENEM
{
	//classe resposável pelo scrollview da tela de busca (ViewController)
	public partial class ScrollView : UIScrollView
	{
		public ScrollView (IntPtr handle) : base (handle)
		{
		}

		public ScrollView(	anoestadoTextField anoTextField,
							anoestadoTextField estadoTextField,
							UITextField municipioTextField, 
							UITextField escolaTextField

		) 
		{
			this.escolaTextField = escolaTextField;
			this.municipioTextField = municipioTextField;
			this.anoTextField = anoTextField;
			this.estadoTextField = estadoTextField;
			this.ScrollEnabled = true;
		}			

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			if (!Dragging) {
				NextResponder.TouchesBegan (touches, evt);
			} else {
				base.TouchesBegan (touches, evt);
			}

		}

		public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			if (!Dragging) {
				NextResponder.TouchesMoved (touches, evt);
			} else {
				base.TouchesMoved (touches, evt);
			}
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			if (!Dragging) {
				NextResponder.TouchesEnded (touches, evt);
			} else {
				base.TouchesEnded (touches, evt);
			}
		}
	}
}
