﻿using System;
using System.Collections.Generic;

using Foundation;

using UIKit;

namespace CheckupENEM
{
	//classe responsável pela inserção das células (UITableViewCells) na UITableView
	//da tela de resultados da busca
	public class TableSource : UITableViewSource
	{
		private List<DadosBusca> _dados;
		private string CellIdentifier = "TableCell";
		public ResultadosTableViewController _controller;

		public TableSource (List<DadosBusca> dados, ResultadosTableViewController controller)
		{
			this._dados = dados;
			this._controller = controller;
		}

		public override nint RowsInSection(UITableView tableView, nint section) 
		{
			return _dados.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			var item = _dados [indexPath.Row];

			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, CellIdentifier);
			}

			cell.TextLabel.Text = item.escola;

			cell.DetailTextLabel.Text = item.detalhe;
			cell.DetailTextLabel.TextColor = UIColor.Blue;
			return cell;
		}

		public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			_controller.dadoSelected = _dados [indexPath.Row];
			_controller.PerformSegue("tableResultadosToPdfSegue", _controller);
		}
	}
}

