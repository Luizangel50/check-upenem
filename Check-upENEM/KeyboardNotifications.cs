﻿using System;
using System.CodeDom.Compiler;

using Foundation;

using UIKit;

using CoreGraphics;

namespace CheckupENEM
{	
	public class KeyboardNotifications
	{
		public UIView View { get; private set;}
		public ScrollView scrollView { get; private set;}
		private UIView activeView;
		private nfloat scroll_amount = 0;
		private nfloat bottom = 0;
		private nfloat offset = 110;
		private bool moveViewUp = false;

		public KeyboardNotifications(UIView View, ScrollView scrollView) {
			this.View = View;
			this.scrollView = scrollView;
		}

		public void KeyBoardUpNotification (NSNotification notification)
		{
			//fechar algum teclado que já esteja aberto
			KeyBoardDownNotification (notification);

			var value = notification.UserInfo.ValueForKey (UIKeyboard.FrameBeginUserInfoKey) as NSValue;

			//tamanho do teclado
			var r = value.CGRectValue;

			//encontrar o campo que abriu o teclado
			if (scrollView != null) {
				foreach (var view in scrollView.Subviews) {
					if (view.IsFirstResponder)
						activeView = view;
				}
			} 
			else {
				foreach (var view in View.Subviews) {
					if (view.IsFirstResponder)
						activeView = view;
				}
			}

			bottom = activeView.Frame.Y + activeView.Frame.Height + offset;

			scroll_amount = r.Height - (View.Frame.Size.Height - bottom);

			if (scroll_amount > 0) {
				moveViewUp = true;
				ScrollTheView (moveViewUp);
			}
			else {
				moveViewUp = false;
			}

		}

		public void KeyBoardDownNotification (NSNotification notification)
		{
			if(moveViewUp){
				ScrollTheView (false);
			}

		}

		private void ScrollTheView(bool move)
		{
			UIView.BeginAnimations (string.Empty, IntPtr.Zero);
			UIView.SetAnimationDuration (0);

			CGRect frame = View.Frame;

			if (move) {
				frame.Y -= scroll_amount;
			}
			else {
				frame.Y += scroll_amount;
				scroll_amount = 0;
			}

			View.Frame = frame;
			UIView.CommitAnimations ();
		}
	}
}

