using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Drawing;

using Foundation;

using UIKit;

using SQLite;

using Xamarin.Forms;

using CoreGraphics;

namespace CheckupENEM
{
	//UIViewController da tela de Busca (Relatório)
	partial class RelatorioBuscaViewController : UIViewController
	{
		public RelatorioBuscaViewController (IntPtr handle) : base (handle)
		{
		}

		//a cada ano, ao adicionar um arquivo .csv, atualizar a lista de anos com o ano adicionado
		private static List<int> anos = new List<int> { 2013, 2014, 2015 }; //Adicionar nesta lista futuros novos anos
		private static List<string> ufsiglas = new List<string> { "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RR", "RO", "RS", "SC", "SE", "SP", "TO" };
		public static List<string> ufnomes = new List<string> { "Acre", "Alagoas", "Amazonas", "Amapá", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goiás", "Maranhão", "Minas Gerais", "Mato Grosso do Sul", "Mato Grosso", "Pará", "Paraíba", "Pernambuco", "Piauí", "Paraná", "Rio de Janeiro", "Rio Grande do Norte", "Roraima", "Rondônia", "Rio Grande do Sul", "Santa Catarina", "Sergipe", "São Paulo", "Tocantins" };

		private int selectedAno = anos[0];
		private string selectedEstado = ufnomes[0];

		//UIPickerViewModels para os campos de busca: Ano e Estado
		private anoPickerViewModel anoModel = new anoPickerViewModel(anos);
		private estadoPickerViewModel estadoModel = new estadoPickerViewModel (ufnomes);
		public UIScrollView scrollBuscaPage;
		public BdAcess bdAcess;

		//keyboard notifications
		private KeyboardNotifications relatorioKeyboard;

		public override void ViewDidLoad ()
		{
			//botão de volta para a tela anterior
			NavigationItem.BackBarButtonItem  = new UIBarButtonItem ("Busca", UIBarButtonItemStyle.Plain, null);
			NavigationItem.SetHidesBackButton (false, true);

			//objeto que realiza as inserções dos dados dos arquivos .csv no banco de dados
			bdAcess = new BdAcess();
			readInsertions(bdAcess);

			base.ViewDidLoad ();

			this.NavigationItem.HidesBackButton = true;

			anoTextField.TextAlignment = UITextAlignment.Center;

			//View.Add (anoTextField);
			scrollBuscaPage = new ScrollView (anoTextField,estadoTextField,municipioTextField,escolaTextField);
			scrollBuscaPage.Frame = new CoreGraphics.CGRect (0, 0, View.Frame.Width, View.Frame.Height);

			View.AddSubview(scrollBuscaPage);
			scrollBuscaPage.AddSubview (imageBackgroundView);
			scrollBuscaPage.AddSubview (procureEscolaLabel);
			scrollBuscaPage.AddSubview (anoLabel);
			scrollBuscaPage.AddSubview (anoTextField);
			scrollBuscaPage.AddSubview (estadoLabel);
			scrollBuscaPage.AddSubview (estadoTextField);
			scrollBuscaPage.AddSubview (municipioLabel);
			scrollBuscaPage.AddSubview (municipioTextField);
			scrollBuscaPage.AddSubview (escolaLabel);
			scrollBuscaPage.AddSubview (escolaTextField);
			scrollBuscaPage.AddSubview (limparButton);
			scrollBuscaPage.AddSubview (buscarButton);

			//observers que registram os eventos aparecer/desaparecer do teclado
			relatorioKeyboard = new KeyboardNotifications(View, View.Subviews[2] as ScrollView);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.DidShowNotification, relatorioKeyboard.KeyBoardUpNotification);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, relatorioKeyboard.KeyBoardDownNotification);

			SetupPicker ();

			estadoTextField.Enabled = false;
			municipioTextField.Enabled = false;
//			municipioTextField.Enabled = true;
			escolaTextField.Enabled = false;
//			escolaTextField.Enabled = true;


			limparButton.TouchUpInside += (sender, e) => {

				estadoTextField.Text = "";
				estadoTextField.Enabled = false;

				municipioTextField.Text = "";
				municipioTextField.Enabled = false;

				escolaTextField.Text = "";
				escolaTextField.Enabled = false;

				buscarButton.Enabled = false;
				buscarButton.SetImage(UIImage.FromFile("search_zoom_dis.png"), UIControlState.Normal);
				buscarButton.SetTitleColor(UIColor.DarkGray, UIControlState.Normal);

				limparButton.Enabled = false;
				limparButton.SetImage(UIImage.FromFile("trash.gif"), UIControlState.Normal);
				limparButton.SetTitleColor(UIColor.DarkGray, UIControlState.Normal);

				anoTextField.Text = "";
				anoTextField.Enabled = false;
				anoTextField.Enabled = true;
			};
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			anoTextField.ResignFirstResponder ();
			estadoTextField.ResignFirstResponder ();
			municipioTextField.ResignFirstResponder ();
			escolaTextField.ResignFirstResponder ();
		}

		// Setup the picker and picker view model
		private void SetupPicker()
		{
			//******************************* Ano ***************************************
			anoTextField.Placeholder = "Selecione o ano";
			anoTextField.TextAlignment = UITextAlignment.Center;

			UIPickerView anoPicker = new UIPickerView();
			anoPicker.ShowSelectionIndicator = true;
			anoPicker.Model = anoModel;

			anoTextField.EditingDidBegin += delegate {
				if(anoTextField.Text.Equals("")) {
					//anoPicker.Model.Selected(anoPicker, 0, 0);
					anoTextField.Text = selectedAno.ToString();
					limparButton.Enabled = true;
					limparButton.SetImage(UIImage.FromFile("trash.png"), UIControlState.Normal);
					limparButton.SetTitleColor(UIColor.FromRGB(243, 146, 0), UIControlState.Normal);
				}
				estadoTextField.TextAlignment = UITextAlignment.Center;
				estadoTextField.Enabled = true;
				estadoTextField.TextAlignment = UITextAlignment.Center;
			};

			anoTextField.EditingDidEnd += delegate {
				//anoTextField.Text = selectedAno.ToString();
				anoTextField.TextAlignment = UITextAlignment.Center;
				//anoTextField.Text = "";
				//anoTextField.Text = selectedAno.ToString();
			};

			anoModel.PickerChanged += (sender, e) => {
				this.selectedAno = e.SelectedValue;
				anoTextField.Text = selectedAno.ToString();
			};

			// Setup the toolbar
			UIToolbar anoToolbar = new UIToolbar();
			anoToolbar.BarStyle = UIBarStyle.Default;
			//toolbar.Translucent = true;
			anoToolbar.SizeToFit();

			//Flexible Button
			UIBarButtonItem flexibleButton = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);

			// Create a 'done' button for the toolbar and add it to the toolbar
			UIBarButtonItem doneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done,
				(s, e) => {
					this.anoTextField.Text = selectedAno.ToString();
					this.anoTextField.ResignFirstResponder();
				});
			anoToolbar.SetItems(new UIBarButtonItem[]{flexibleButton, doneButton}, true);

			// Tell the textbox to use the picker for input
			this.anoTextField.InputView = anoPicker;

			// Display the toolbar over the pickers
			this.anoTextField.InputAccessoryView = anoToolbar;

			//Colocando a imagem da seta
			anoTextField.RightViewMode = UITextFieldViewMode.Always;
			anoTextField.RightView = new UIImageView(UIImage.FromFile("arrowdown.gif"));


			//******************************* Estado ***************************************
			estadoTextField.Placeholder = "Selecione o estado";
			estadoTextField.TextAlignment = UITextAlignment.Center;
			//estadoTextField.VerticalAlignment = UIControlContentVerticalAlignment.Center;

			UIPickerView estadoPicker = new UIPickerView();
			estadoPicker.ShowSelectionIndicator = true;
			estadoPicker.Model = estadoModel;

			estadoTextField.EditingDidBegin += delegate {
				//scrollBuscaPage.SetContentOffset(bottom, true);
				if(estadoTextField.Text.Equals("")) {

					buscarButton.Enabled = true;
					buscarButton.SetImage(UIImage.FromFile("search_zoom.png"), UIControlState.Normal);
					buscarButton.SetTitleColor(UIColor.White, UIControlState.Normal);
					//estadoPicker.Model.Selected(estadoPicker, 0, 0);
					estadoTextField.TextAlignment = UITextAlignment.Center;

					estadoTextField.Text = selectedEstado;
					estadoTextField.TextAlignment = UITextAlignment.Center;
				}

				municipioTextField.Enabled = true;
				escolaTextField.Enabled = true;
			};

			estadoTextField.EditingDidEnd += delegate {
				estadoTextField.Text = selectedEstado;
				estadoTextField.TextAlignment = UITextAlignment.Center;
				estadoTextField.Text = "";
				estadoTextField.Text = selectedEstado;
			};

			estadoModel.estadoPickerChanged += (sender, e) => {
				this.selectedEstado = e.SelectedValue;
				estadoTextField.Text = selectedEstado;
			};

			// Setup the toolbar
			UIToolbar estadoToolbar = new UIToolbar();
			estadoToolbar.BarStyle = UIBarStyle.Default;
			//toolbar.Translucent = true;
			estadoToolbar.SizeToFit();

			// Create a 'done' button for the toolbar and add it to the toolbar
			UIBarButtonItem estadoDoneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done,
				(s, e) => {
					this.estadoTextField.Text = selectedEstado;
					this.estadoTextField.ResignFirstResponder();
				});
			estadoToolbar.SetItems(new UIBarButtonItem[]{flexibleButton, estadoDoneButton}, true);

			// Tell the textbox to use the picker for input
			this.estadoTextField.InputView = estadoPicker;

			// Display the toolbar over the pickers
			this.estadoTextField.InputAccessoryView = estadoToolbar;

			//Colocando a imagem da seta
			estadoTextField.RightViewMode = UITextFieldViewMode.Always;
			estadoTextField.RightView = new UIImageView(UIImage.FromFile("arrowdown.gif"));



			//******************************* Municipio e Escola ***************************************
			//Configurando placeholder dos campos município e escola
			municipioTextField.Placeholder = "Município (opcional)";
			escolaTextField.Placeholder = "Escola (opcional)";				
		}

		//Ler as linhas dos arquivos BaseDadosEnem para inserir
		//os dados no banco de dados
		private void readInsertions(BdAcess acesso)
		{
			acesso.buildTables(anos, ufsiglas, ufnomes);
			List<string> insertions = new List<string>();
			string line;

			string arquivo;
			var assembly = typeof(AppDelegate).GetTypeInfo().Assembly;
			System.IO.Stream stream;
			foreach (int ano in anos)
			{
				arquivo = "CheckupENEM.Resources." + "BaseDadosEnem" + ano + ".csv";
				stream = assembly.GetManifestResourceStream(arquivo);

				using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
				{
					while ((line = sr.ReadLine()) != null)
					{
						insertions.Add("insert into [geral] values (" + line + ");");
					}
				}
				acesso.access(ano, insertions);
				insertions.Clear();
			}          
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender) {
			base.PrepareForSegue (segue, sender);

			var resultadosTableViewController = segue.DestinationViewController as ResultadosTableViewController;

			var anosTable = bdAcess.connection.Table<Ano> ();
			var estadosTable = bdAcess.connection.Table<UF> ();

			if (resultadosTableViewController != null) {
				resultadosTableViewController.paths = bdAcess._paths;
				resultadosTableViewController.ano = anosTable.ElementAt (anos.IndexOf (selectedAno));
				resultadosTableViewController.estado = estadosTable.ElementAt (ufnomes.IndexOf (selectedEstado));
				resultadosTableViewController.municipio = municipioTextField.Text;
				resultadosTableViewController.escola = escolaTextField.Text;
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}
