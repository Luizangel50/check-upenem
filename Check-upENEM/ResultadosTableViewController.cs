using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;

using SQLite;

using Mono.Data.Sqlite;

//Package used to remove diacritics
using MMLib.Extensions;

using iTextSharp.text;
using iTextSharp.text.pdf;

using Xamarin.Forms;

using UIKit;

namespace CheckupENEM
{
	//classe responsável pela tela com a UITableView de resultados da busca
	public partial class ResultadosTableViewController : UIViewController
	{
		public ResultadosTableViewController (IntPtr handle) : base (handle)
		{
		}

		UITableViewController tableController = new UITableViewController ();

		public List<string> paths;
		public SqliteConnection connection = new SqliteConnection();
		public SqliteCommand command = new SqliteCommand ();

		private List<DadosBusca> dados = new List<DadosBusca> ();
		public DadosBusca dadoSelected;

		public string municipio;
		public string escola;

		public Ano ano;
		public UF estado;

		private static bool isMunicipioEmpty = false;
		private static bool isEscolaEmpty = false;

		public static bool getMunicipioEmpty(){
			return isMunicipioEmpty;
		}

		public static bool getEscolaEmpty(){
			return isEscolaEmpty;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			NavigationItem.BackBarButtonItem  = new UIBarButtonItem ("Escolas", UIBarButtonItemStyle.Plain, null);
			NavigationItem.SetHidesBackButton (false, true);

			fillLabels(ano, estado, municipio, escola);
		}

		void fillLabels(Ano ano, UF estado, string municipio, string escola)
		{
			campoResultadosTextView.Text += "Ano: " + ano.ANO_NUMERO;
			campoResultadosTextView.Text += Environment.NewLine + "Estado: " + estado.UNIDF_NOME;

			if (!(municipio.IsNullOrWhiteSpace () || municipio.IsNullOrWhiteSpace ())) {
				municipio = municipio.RemoveDiacritics ().Trim ();
				campoResultadosTextView.Text += Environment.NewLine + "Municipio: " + municipio;
			}

			if (!(escola.IsNullOrEmpty () || escola.IsNullOrWhiteSpace ())) {
				escola = escola.RemoveDiacritics ().Trim ();
				campoResultadosTextView.Text += Environment.NewLine + "Escola: " + escola;
			}

			string queryMunicipio =	"SELECT * FROM [geral] WHERE " +
				"SIGLA_DA_UF LIKE '%" + estado.UNIDF_SIGLA + "%' " +
				"AND MUNICIPIO LIKE '%" + municipio + "%' ";

			string queryEscola = queryMunicipio + 	"AND ANO LIKE '%" + ano.ANO + "%' " + 
				"AND NOME_DA_ENTIDADE LIKE '%" + escola + "%' " + 
				"ORDER BY NOME_DA_ENTIDADE ASC";

			dados.Clear();

			foreach (var path in paths) {

				connection.ConnectionString = "Data Source=" + path + ";Version=3;";

				command.Connection = connection;
				command.CommandText = queryMunicipio;
				connection.Open ();

				SqliteDataReader r = command.ExecuteReader ();

				//Verifica se há municípios com o nome buscado
				if(!r.Read() && path.Contains(ano.ANO_NUMERO.ToString()))
					isMunicipioEmpty = true;
				else if(r.Read() && path.Contains(ano.ANO_NUMERO.ToString()))
					isMunicipioEmpty = false;

				r.Close ();

				command.CommandText = queryEscola;
				r = command.ExecuteReader ();

				while (r.Read ()) {
					dados.Add (new DadosBusca () {   
						escola = r.GetString (r.GetOrdinal ("NOME_DA_ENTIDADE")),

						ano = ano.ANO_NUMERO.ToString(),
						codigo = r.GetInt32 (r.GetOrdinal ("_id")).ToString(),
						municipio = r.GetString (r.GetOrdinal ("MUNICIPIO")),
						unidfed = r.GetString (r.GetOrdinal ("SIGLA_DA_UF")),
						detalhe = 	"Código: " + 	r.GetInt32 (r.GetOrdinal ("_id")).ToString() + ", " + 
													r.GetString (r.GetOrdinal ("MUNICIPIO")) + ", " + 
													r.GetString (r.GetOrdinal ("SIGLA_DA_UF")) + ", " + 
													ano.ANO_NUMERO.ToString(),
						media_red = r.GetDecimal(r.GetOrdinal("MEDIA_RED")),
						media_lc = r.GetDecimal(r.GetOrdinal("MEDIA_LC")),
						media_mat = r.GetDecimal(r.GetOrdinal("MEDIA_MAT")),
						media_ch = r.GetDecimal(r.GetOrdinal("MEDIA_CH")),
						media_cn = r.GetDecimal(r.GetOrdinal("MEDIA_CN")),
						media_geral_sem_red = r.GetDecimal(r.GetOrdinal("MEDIA_GERAL_SEM_RED")),
						classificacao_nacional = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_NACIONAL")),
						classificacao_estadual = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_ESTADUAL")),
						classificacao_municipal = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_MUNICIPAL")),
						classificacao_nacional_red = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_NACIONAL_RED")),
						classificacao_estadual_red = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_ESTADUAL_RED")),
						classificacao_municipal_red = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_MUNICIPAL_RED")),
						classificacao_nacional_lc = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_NACIONAL_LC")),
						classificacao_estadual_lc = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_ESTADUAL_LC")),
						classificacao_municipal_lc = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_MUNICIPAL_LC")),
						classificacao_nacional_mat = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_NACIONAL_MAT")),
						classificacao_estadual_mat = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_ESTADUAL_MAT")),
						classificacao_municipal_mat = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_MUNICIPAL_MAT")),
						classificacao_nacional_ch = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_NACIONAL_CH")),
						classificacao_estadual_ch = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_ESTADUAL_CH")),
						classificacao_municipal_ch = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_MUNICIPAL_CH")),
						classificacao_nacional_cn = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_NACIONAL_CN")),
						classificacao_estadual_cn = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_ESTADUAL_CN")),
						classificacao_municipal_cn = r.GetInt32(r.GetOrdinal("CLASSIFICACAO_MUNICIPAL_CN"))
					});


				}

				//Verifica se há escolas com o nome buscado
				if (dados.Count == 0 && path.Contains(ano.ANO_NUMERO.ToString()))
					isEscolaEmpty = true;
				else if(dados.Count != 0 && path.Contains(ano.ANO_NUMERO.ToString()))
					isEscolaEmpty = false;

				r.Close ();

				connection.Close ();
			}
				
			resultadosTableView.Source = new TableSource (dados, this);
			resultadosTableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			tableController.TableView = resultadosTableView;

			View.AddSubview (resultadosTableView);

			if (isMunicipioEmpty) {
				var alert = new UIAlertView () { 
					Title = "Check-up ENEM", 
					Message = "Município não encontrado!",
				};
				alert.AddButton ("OK");
				alert.Show ();
			} 
			else if (isEscolaEmpty) {
				var alert = new UIAlertView () { 
					Title = "Check-up ENEM", 
					Message = "Escola não encontrada!",
				};
				alert.AddButton ("OK");
				alert.Show ();
			}
		}
			
		public override void PrepareForSegue (UIStoryboardSegue segue, Foundation.NSObject sender)
		{
			if (segue.Identifier.Equals("tableResultadosToPdfSegue")) {
				base.PrepareForSegue (segue, sender);

				var pdfWebViewController = segue.DestinationViewController as WebViewController;

				if (pdfWebViewController != null) {
					pdfWebViewController.dados = dadoSelected;
				}
			}
		}
	}
}
