// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace CheckupENEM
{
	[Register ("ResultadosTableViewController")]
	partial class ResultadosTableViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView campoResultadosTextView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView resultadosTableView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (campoResultadosTextView != null) {
				campoResultadosTextView.Dispose ();
				campoResultadosTextView = null;
			}
			if (resultadosTableView != null) {
				resultadosTableView.Dispose ();
				resultadosTableView = null;
			}
		}
	}
}
