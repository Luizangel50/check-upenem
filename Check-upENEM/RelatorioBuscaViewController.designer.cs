// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace CheckupENEM
{
	[Register ("RelatorioBuscaViewController")]
	partial class RelatorioBuscaViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel anoLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		anoestadoTextField anoTextField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton buscarButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel escolaLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField escolaTextField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel estadoLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		anoestadoTextField estadoTextField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView imageBackgroundView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton limparButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel municipioLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField municipioTextField { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel procureEscolaLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITabBarItem relatorioTabBarItem { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (anoLabel != null) {
				anoLabel.Dispose ();
				anoLabel = null;
			}
			if (anoTextField != null) {
				anoTextField.Dispose ();
				anoTextField = null;
			}
			if (buscarButton != null) {
				buscarButton.Dispose ();
				buscarButton = null;
			}
			if (escolaLabel != null) {
				escolaLabel.Dispose ();
				escolaLabel = null;
			}
			if (escolaTextField != null) {
				escolaTextField.Dispose ();
				escolaTextField = null;
			}
			if (estadoLabel != null) {
				estadoLabel.Dispose ();
				estadoLabel = null;
			}
			if (estadoTextField != null) {
				estadoTextField.Dispose ();
				estadoTextField = null;
			}
			if (imageBackgroundView != null) {
				imageBackgroundView.Dispose ();
				imageBackgroundView = null;
			}
			if (limparButton != null) {
				limparButton.Dispose ();
				limparButton = null;
			}
			if (municipioLabel != null) {
				municipioLabel.Dispose ();
				municipioLabel = null;
			}
			if (municipioTextField != null) {
				municipioTextField.Dispose ();
				municipioTextField = null;
			}
			if (procureEscolaLabel != null) {
				procureEscolaLabel.Dispose ();
				procureEscolaLabel = null;
			}
			if (relatorioTabBarItem != null) {
				relatorioTabBarItem.Dispose ();
				relatorioTabBarItem = null;
			}
		}
	}
}
