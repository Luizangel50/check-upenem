using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Threading.Tasks;

using Foundation;

using UIKit;

using MessageUI;

using Xamarin.Forms;

namespace CheckupENEM
{
	//classe responsável pela tela em que aparece o PDF do relatório
	partial class WebViewController : UIViewController
	{
		private string relatorioEnemFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "RelatorioEnem.pdf");

		public DadosBusca dados;

		public WebViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			webViewPDF = new UIWebView (View.Bounds);

			View.AddSubview (webViewPDF);

			FormResultados.modifyPdf (relatorioEnemFile, dados);
			webViewPDF.LoadRequest(new NSUrlRequest(new NSUrl(relatorioEnemFile, false)));

			webViewPDF.ScalesPageToFit = true;
			webViewPDF.ClipsToBounds = true;
			webViewPDF.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;

			var archive = NSObject.FromObject(NSData.FromFile(relatorioEnemFile));
			var item = new NSObject[] { archive };

			var barButton = new UIBarButtonItem (UIBarButtonSystemItem.Action);

			barButton.Clicked += delegate {
				//verificando se o dispositivo é iPad ou iPhone (não funciona no simulador do MAC)
				var activityController = new UIActivityViewController (item, null);
//				activityController.ExcludedActivityTypes = new  NSString[] {UIActivityType.AssignToContact, 
//																			UIActivityType.Message,
//																			UIActivityType.PostToFacebook,
//																			UIActivityType.PostToTwitter,
//																			UIActivityType.PostToWeibo,
//																			UIActivityType.CopyToPasteboard,
//																			UIActivityType.SaveToCameraRoll};
				if(iOS.Hardware.DeviceHardware.HardwareModel.Contains("iPad")) {
					var pop = new UIPopoverController(activityController);
					PresentViewController (activityController, true, () => {});
					pop.PresentFromBarButtonItem(barButton, UIPopoverArrowDirection.Any, true);
				}

				else if(iOS.Hardware.DeviceHardware.HardwareModel.Contains("iPhone")){
					PresentViewController(activityController, true, null);
				}


				//Outra opção de mostrar a funcionalidade share com ActionSheet para iPhone
//				else if(iOS.Hardware.DeviceHardware.HardwareModel.Contains("iPhone")) {
//					//var action = Xamarin.Forms.Page.DisplayActionSheet ("Imprimir ou Compartilhar", "Cancel", null, "Imprimir", "Email");
//
//					UIAlertController actionSheetAlert = UIAlertController.Create("Imprimir ou Compartilhar", "Selecione uma opção abaixo", UIAlertControllerStyle.ActionSheet);
//
//					// Add Actions
//					actionSheetAlert.AddAction(UIAlertAction.Create("Imprimir",UIAlertActionStyle.Default, (action) => {
//						var printInfo = UIPrintInfo.PrintInfo;
//
//						printInfo.Duplex = UIPrintInfoDuplex.LongEdge;
//
//						printInfo.OutputType = UIPrintInfoOutputType.General;
//
//						printInfo.JobName = "Relatório";
//
//						var printer = UIPrintInteractionController.SharedPrintController;
//
//						printer.PrintInfo = printInfo;
//
//						printer.PrintingItem = NSData.FromFile(relatorioEnemFile);
//
//						printer.ShowsPageRange = true;
//
//						printer.Present(true, (handler, completed, err) => {
//							if(!completed && err !=null) {
//								Console.WriteLine("Printer Error");
//							}
//						});
//					}));
//
//
//					actionSheetAlert.AddAction(UIAlertAction.Create("Email",UIAlertActionStyle.Default, (action) => {
//						MFMailComposeViewController mailController;
//
//						if(MFMailComposeViewController.CanSendMail) {
//
//							mailController = new MFMailComposeViewController ();
//							//mailController.SetToRecipients (new string[]{"john@doe.com"});
//							mailController.SetSubject ("Relatório Enem de " + dados.ano);
//							mailController.SetMessageBody ("Segue em anexo o Relatório do Enem da escola " + dados.escola, false);
//							mailController.AddAttachmentData (NSData.FromFile (relatorioEnemFile), @"application/pdf", "Formulario_Enem_" + 
//								dados.ano + "_" + 
//								dados.escola + ".pdf");
//							mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
//								//Console.WriteLine (args.Result.ToString ());
//								args.Controller.DismissViewController (true, null);
//							};
//
//							UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController (mailController, true, null);
//						}
//
//					}));
//
//					UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
//					if (presentationPopover!=null) {
//						presentationPopover.SourceView = this.View;
//						presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Down;
//					}
//
//					// Display the alert
//					this.PresentViewController(actionSheetAlert,true,null);
//				}
			};

			this.NavigationItem.SetRightBarButtonItem (barButton, true);
		}
	}
}
