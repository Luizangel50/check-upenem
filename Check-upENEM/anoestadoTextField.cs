using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace CheckupENEM
{
	public partial class anoestadoTextField : UITextField
	{
		public anoestadoTextField (IntPtr handle) : base (handle)
		{
		}

		public override bool CanPerform(ObjCRuntime.Selector action, NSObject withSender)
		{
			if (action == new ObjCRuntime.Selector ("paste:"))
				return false;
			else if (action == new ObjCRuntime.Selector ("selectAll:"))
				return false;
			else if (action == new ObjCRuntime.Selector ("copy:"))
				return false;
			else if (action == new ObjCRuntime.Selector ("select:"))
				return false;
			else if (action == new ObjCRuntime.Selector ("cut:"))
				return false;
			else
				return base.CanPerform(action, withSender);
		}
	}
}
