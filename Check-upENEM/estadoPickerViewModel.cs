﻿using System;
using System.Collections.Generic;
using CoreGraphics;

using UIKit;

namespace CheckupENEM
{
	public class estadoPickerViewModel : UIPickerViewModel
	{
		private List<string> _ufnomes;

		public estadoPickerViewModel(List<string> ufnomes) {
			_ufnomes = ufnomes;
		}
		public event EventHandler<estadoPickerChangedEventArgs> estadoPickerChanged;

		public override void Selected (UIPickerView picker, nint row, nint component)
		{
			if (this.estadoPickerChanged != null)
			{
				this.estadoPickerChanged(this, new estadoPickerChangedEventArgs{SelectedValue = _ufnomes[(int)row]});
			}
		}

		public override nint GetComponentCount (UIPickerView picker)
		{
			return 1;
		}

		public override nint GetRowsInComponent (UIPickerView picker, nint component)
		{
			return _ufnomes.Count;
		}

		public override string GetTitle (UIPickerView picker, nint row, nint component)
		{
			return _ufnomes[(int)row];
		}
			
		public override nfloat GetRowHeight (UIPickerView picker, nint component)
		{
			return 30f;
		}

	}

	public class estadoPickerChangedEventArgs : EventArgs
	{
		public string SelectedValue {get; set;}
	}
}