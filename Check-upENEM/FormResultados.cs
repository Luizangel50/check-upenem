﻿using System;
using System.IO;

using System.Collections.Generic;
using System.Reflection;

using Foundation;

using iTextSharp.text.pdf;

using UIKit;

using Xamarin.Forms;

using CheckupENEM;

[assembly: Dependency(typeof(FormResultados))]

namespace CheckupENEM
{
	//classe responsável pela modificação do formulário PDF para gerar o relatório
	public class FormResultados
	{
		public static void modifyPdf(string filePath, DadosBusca selectedCell) {
			// Aggregate successive pages here:
			//var pagesAll = new List<byte[]>();

			// Hold individual pages Here:
			//byte[] pageBytes = null;

			var assembly = typeof(FormResultados).GetTypeInfo().Assembly;
			string templatePath = "CheckupENEM.Resources." + "FormularioRelatorioEnem" + ".pdf";

			using (System.IO.Stream stream = assembly.GetManifestResourceStream (templatePath)) {

				PdfReader reader = new PdfReader (stream);

				using (var tempStream = new System.IO.MemoryStream ()) {

					// Create a new DirectoryInfo object.
//					DirectoryInfo dInfo = new DirectoryInfo(filePath);

					// Get a DirectorySecurity object that represents the 
					// current security settings.
//					System.Security.AccessControl.DirectorySecurity dSecurity = dInfo.GetAccessControl();
//
					// Add the FileSystemAccessRule to the security settings. 
//					dSecurity.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule(Account,
//						Rights,
//						ControlType));
//
					// Set the new access settings.
//					dInfo.SetAccessControl(dSecurity);
					PdfStamper stamper = new PdfStamper (reader, new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite));
					stamper.FormFlattening = true;
					AcroFields fields = stamper.AcroFields;
					stamper.Writer.CloseStream = false;

					// Grab a reference to the Dictionary in the current merge item:
					stamper.AcroFields.SetField ("ano", "ENEM " + selectedCell.ano);
					stamper.AcroFields.SetField ("codigo", selectedCell.codigo);
					stamper.AcroFields.SetField ("escola", selectedCell.escola);
					stamper.AcroFields.SetField ("municipio", selectedCell.municipio);
					stamper.AcroFields.SetField ("uf", selectedCell.unidfed);
					stamper.AcroFields.SetField ("class_br", selectedCell.classificacao_nacional.ToString()+ "º");
					stamper.AcroFields.SetField ("class_br_red", selectedCell.classificacao_nacional_red.ToString()+ "º");
					stamper.AcroFields.SetField ("class_br_lc", selectedCell.classificacao_nacional_lc.ToString()+ "º");
					stamper.AcroFields.SetField ("class_br_mat", selectedCell.classificacao_nacional_mat.ToString()+ "º");
					stamper.AcroFields.SetField ("class_br_ch", selectedCell.classificacao_nacional_ch.ToString()+ "º");
					stamper.AcroFields.SetField ("class_br_cn", selectedCell.classificacao_nacional_cn.ToString()+ "º");
					stamper.AcroFields.SetField ("nota", selectedCell.media_geral_sem_red.ToString("#.##"));
					stamper.AcroFields.SetField ("nota_red", selectedCell.media_red.ToString("#.##"));
					stamper.AcroFields.SetField ("nota_lc", selectedCell.media_lc.ToString("#.##"));
					stamper.AcroFields.SetField ("nota_mat", selectedCell.media_mat.ToString("#.##"));
					stamper.AcroFields.SetField ("nota_ch", selectedCell.media_ch.ToString("#.##"));
					stamper.AcroFields.SetField ("nota_cn", selectedCell.media_cn.ToString("#.##"));
					stamper.AcroFields.SetField ("class_est", selectedCell.classificacao_estadual.ToString()+ "º");
					stamper.AcroFields.SetField ("class_est_red", selectedCell.classificacao_estadual_red.ToString()+ "º");
					stamper.AcroFields.SetField ("class_est_lc", selectedCell.classificacao_estadual_lc.ToString()+ "º");
					stamper.AcroFields.SetField ("class_est_mat", selectedCell.classificacao_estadual_mat.ToString()+ "º");
					stamper.AcroFields.SetField ("class_est_ch", selectedCell.classificacao_estadual_ch.ToString()+ "º");
					stamper.AcroFields.SetField ("class_est_cn", selectedCell.classificacao_estadual_cn.ToString()+ "º");
					stamper.AcroFields.SetField ("class_mun", selectedCell.classificacao_municipal.ToString()+ "º");
					stamper.AcroFields.SetField ("class_mun_red", selectedCell.classificacao_municipal_red.ToString()+ "º");
					stamper.AcroFields.SetField ("class_mun_lc", selectedCell.classificacao_municipal_lc.ToString()+ "º");
					stamper.AcroFields.SetField ("class_mun_mat", selectedCell.classificacao_municipal_mat.ToString()+ "º");
					stamper.AcroFields.SetField ("class_mun_ch", selectedCell.classificacao_municipal_ch.ToString()+ "º");
					stamper.AcroFields.SetField ("class_mun_cn", selectedCell.classificacao_municipal_cn.ToString()+ "º");

					// If we had not set the CloseStream property to false, 
					// this line would also kill our memory stream:
					stamper.Close ();

					// Reset the stream position to the beginning before reading:
					//tempStream.Position = 0;

					// Grab the byte array from the temp stream . . .
					//pageBytes = tempStream.ToArray ();

					// And add it to our array of all the pages:
					//pagesAll.Add (pageBytes);
				}
				reader.Close ();
			}
		}
	}
}

