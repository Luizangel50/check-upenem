﻿using SQLite;

//Classes utilizadas para armazenamento no banco dos dados dos arquivos .csv
//e armazenamento dos dados das buscas
namespace CheckupENEM
{
	[Table("geral")]
	public class BaseGeral {
		[PrimaryKey, Column("_id"),NotNull]
		public int CODIGO_DA_ENTIDADE { get; set;}

		[NotNull]
		public int ANO { get; set;}

		[MaxLength(255), NotNull]
		public string NOME_DA_ENTIDADE { get; set;}

		[MaxLength(2),NotNull]
		public string SIGLA_DA_UF { get; set;}

		[MaxLength(255), NotNull]
		public string MUNICIPIO { get; set;}

		[NotNull]
		public decimal MEDIA_RED { get; set;}

		[NotNull]
		public decimal MEDIA_LC { get; set;}

		[NotNull]
		public decimal MEDIA_MAT { get; set;}

		[NotNull]
		public decimal MEDIA_CH { get; set;}

		[NotNull]
		public decimal MEDIA_CN { get; set;}

		[NotNull]
		public decimal MEDIA_GERAL_SEM_RED { get; set;}

		[NotNull]
		public int CLASSIFICACAO_NACIONAL { get; set;}

		[NotNull]
		public int CLASSIFICACAO_ESTADUAL { get; set;}

		[NotNull]
		public int CLASSIFICACAO_MUNICIPAL { get; set;}

		[NotNull]
		public int CLASSIFICACAO_NACIONAL_RED { get; set;}

		[NotNull]
		public int CLASSIFICACAO_ESTADUAL_RED { get; set;}

		[NotNull]
		public int CLASSIFICACAO_MUNICIPAL_RED { get; set;}

		[NotNull]
		public int CLASSIFICACAO_NACIONAL_LC { get; set;}

		[NotNull]
		public int CLASSIFICACAO_ESTADUAL_LC { get; set;}

		[NotNull]
		public int CLASSIFICACAO_MUNICIPAL_LC { get; set;}

		[NotNull]
		public int CLASSIFICACAO_NACIONAL_MAT { get; set;}

		[NotNull]
		public int CLASSIFICACAO_ESTADUAL_MAT { get; set;}

		[NotNull]
		public int CLASSIFICACAO_MUNICIPAL_MAT { get; set;}

		[NotNull]
		public int CLASSIFICACAO_NACIONAL_CH { get; set;}

		[NotNull]
		public int CLASSIFICACAO_ESTADUAL_CH { get; set;}

		[NotNull]
		public int CLASSIFICACAO_MUNICIPAL_CH { get; set;}

		[NotNull]
		public int CLASSIFICACAO_NACIONAL_CN { get; set;}

		[NotNull]
		public int CLASSIFICACAO_ESTADUAL_CN { get; set;}

		[NotNull]
		public int CLASSIFICACAO_MUNICIPAL_CN { get; set;}
	}

	[Table("ano")]
	public class Ano {
		[PrimaryKey, AutoIncrement, Column("_id"), NotNull]
		public int ANO { get; set;}

		[NotNull, Unique]
		public int ANO_NUMERO { get; set; }

	}

	[Table("uf")]
	public class UF {
		[PrimaryKey, AutoIncrement, Column("_id"), NotNull]
		public int UNIDF { get; set; }

		[MaxLength(2),NotNull,Unique]
		public string UNIDF_SIGLA { get; set;}

		[NotNull,Unique]
		public string UNIDF_NOME { get; set; }
	}


	public class DadosBusca
	{
		public string escola { get; set; }

		public string ano { get; set; }

		public string codigo { get; set; }

		public string municipio { get; set; }

		public string unidfed { get; set; }

		public string detalhe { get; set;}

		public decimal media_red { get; set;}

		public decimal media_lc { get; set;}

		public decimal media_mat { get; set;}

		public decimal media_ch { get; set;}

		public decimal media_cn { get; set;}

		public decimal media_geral_sem_red { get; set;}

		public int classificacao_nacional { get; set;}

		public int classificacao_estadual { get; set;}

		public int classificacao_municipal { get; set;}

		public int classificacao_nacional_red { get; set;}

		public int classificacao_estadual_red { get; set;}

		public int classificacao_municipal_red { get; set;}

		public int classificacao_nacional_lc { get; set;}

		public int classificacao_estadual_lc { get; set;}

		public int classificacao_municipal_lc { get; set;}

		public int classificacao_nacional_mat { get; set;}

		public int classificacao_estadual_mat { get; set;}

		public int classificacao_municipal_mat { get; set;}

		public int classificacao_nacional_ch { get; set;}

		public int classificacao_estadual_ch { get; set;}

		public int classificacao_municipal_ch { get; set;}

		public int classificacao_nacional_cn { get; set;}

		public int classificacao_estadual_cn { get; set;}

		public int classificacao_municipal_cn { get; set;}

	}
}
